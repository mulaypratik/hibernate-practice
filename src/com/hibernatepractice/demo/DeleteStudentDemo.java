package com.hibernatepractice.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernatepractice.entity.Student;

public class DeleteStudentDemo {

	public static void main(String args[]) {
		// Create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// Create session
		Session session = factory.getCurrentSession();

		try {
			int intStudentId = 7;

			// Start a transaction
			session.beginTransaction();

			Student student = session.get(Student.class, intStudentId);
			System.out.println("Fetched Student of id: " + intStudentId + " is " + student);

			// Delete the Student(s)
			System.out.println("Deleting the Student(s)...");
			session.delete(student);

			// Commit transaction
			session.getTransaction().commit();

			// ANOTHER METHOD
			intStudentId = 3;

			session = factory.getCurrentSession();

			// Start a transaction
			session.beginTransaction();

			student = session.get(Student.class, intStudentId);
			System.out.println("Fetched Student of id: " + intStudentId + " is " + student);

			// Delete the Student(s)
			System.out.println("Deleting the Student(s)...");
			session.createQuery("delete from Student where id = '" + intStudentId + "'").executeUpdate();

			session.getTransaction().commit();

			System.out.println("Done!");
		} finally {
			factory.close();
		}
	}
}
