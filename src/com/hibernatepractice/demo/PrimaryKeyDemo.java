package com.hibernatepractice.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernatepractice.entity.Student;

public class PrimaryKeyDemo {

	public static void main(String[] args) {
		// Create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// Create session
		Session session = factory.getCurrentSession();

		try {
			// Use the session object to save Java object --
			// Create 3 Student objects
			System.out.println("Creating a new Student object...");
			Student student1 = new Student("Pratik", "Mulay", "mulaypratik@yahoo.co.in");
			Student student2 = new Student("Kevin", "George", "kevin@cmss.in");
			Student student3 = new Student("Mayur", "Joshi", "mayur@cmss.in");

			// Start a transaction
			session.beginTransaction();

			// Save the Student objects
			System.out.println("Saving the Student...");
			session.save(student1);
			session.save(student2);
			session.save(student3);

			// Commit transaction
			session.getTransaction().commit();
			System.out.println("Done!");
			// --
		} finally {
			factory.close();
		}
	}

}
