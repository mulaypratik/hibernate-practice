package com.hibernatepractice.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernatepractice.entity.Student;

public class QueryStudentDemo {

	@SuppressWarnings("unchecked")
	public static void main(String args[]) {
		// Create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// Create session
		Session session = factory.getCurrentSession();

		try {
			// Start a transaction
			session.beginTransaction();

			// Query all students
			List<Student> lstStudent = session.createQuery("from Student").getResultList();

			// Display the students
			displayStudents(lstStudent);

			// Query students with last name='Bond'
			lstStudent = session.createQuery("from Student where strLastName='Bond'").getResultList();

			// Display the students
			System.out.println("\n\nStudents with last name='Bond':");
			displayStudents(lstStudent);

			// Students with firstname like 'P%' or lastname like '%i'
			lstStudent = session.createQuery("from Student where strFirstName like 'P%' or strLastName like '%i'")
					.getResultList();

			// Display the students
			System.out.println("\n\nStudents with firstname like 'P%' or lastname like '%i':");
			displayStudents(lstStudent);

			// Commit transaction
			session.getTransaction().commit();
			System.out.println("Done!");
			// --
		} finally {
			factory.close();
		}
	}

	private static void displayStudents(List<Student> lstStudent) {
		for (Student student : lstStudent) {
			System.out.println(student);
		}
	}
}
