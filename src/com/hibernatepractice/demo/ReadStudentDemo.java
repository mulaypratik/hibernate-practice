package com.hibernatepractice.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernatepractice.entity.Student;

public class ReadStudentDemo {

	public static void main(String args[]) {
		// Create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// Create session
		Session session = factory.getCurrentSession();

		try {
			// Use the session object to save Java object --
			// Create a Student object
			System.out.println("Creating a new Student object...");
			Student student = new Student("James", "Bond", "james@spy.uk");

			// Start a transaction
			session.beginTransaction();

			// Save the Student object
			System.out.println("Saving the Student...");
			session.save(student);

			// Commit transaction
			session.getTransaction().commit();
			// --

			// NEW CODE
			System.out.println("Saved Student. Generated id: " + student.getIntId());

			// Now get a new session and start transaction
			session = factory.getCurrentSession();
			session.beginTransaction();

			// Retrieve Student based on id (PK)
			System.out.println("Getting Student with id:" + student.getIntId());

			Student studentNew = session.get(Student.class, student.getIntId());
			System.out.println("Get complete: " + studentNew);

			// Commit the transaction
			session.getTransaction().commit();

			System.out.println("Done!");
		} finally {
			factory.close();
		}
	}
}
