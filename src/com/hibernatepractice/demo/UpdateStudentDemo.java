package com.hibernatepractice.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernatepractice.entity.Student;

public class UpdateStudentDemo {

	public static void main(String args[]) {
		// Create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// Create session
		Session session = factory.getCurrentSession();

		try {
			int intStudentId = 7;

			// Start a transaction
			session.beginTransaction();

			Student student = session.get(Student.class, intStudentId);
			System.out.println("Fetched Student of id: " + intStudentId + " is " + student);

			// Update the Student's first name
			System.out.println("Updating the Student...");
			student.setStrFirstName("Peter");

			// Commit transaction
			session.getTransaction().commit();

			// ANOTHER METHOD
			session = factory.getCurrentSession();

			// Start a transaction
			session.beginTransaction();

			student = session.get(Student.class, intStudentId);
			System.out.println("Fetched Student of id: " + intStudentId + " is " + student);

			// Update email for specific student
			System.out.println("Update email for specific student");

			session.createQuery("update Student set strEmail = 'james@gmail.com' where id = '" + intStudentId + "'")
					.executeUpdate();

			session.getTransaction().commit();

			System.out.println("Done!");
		} finally {
			factory.close();
		}
	}
}
