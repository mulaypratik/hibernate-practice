package com.hibernatepractice.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestJdbc {

	public static void main(String[] args) {
		String strJdbcUrl = "jdbc:mysql://localhost:3306/hb_student_tracker?useSSL=false";
		String strUserName = "hbstudent";
		String strPassword = "hbstudent";

//		String strUserName = "root";
//		String strPassword = "root";

		System.out.println("Connecting to the database: " + strJdbcUrl);

		Connection con = null;

		try {
			con = DriverManager.getConnection(strJdbcUrl, strUserName, strPassword);

			System.out.println("Connection successful!");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
